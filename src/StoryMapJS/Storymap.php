<?php

namespace Drupal\views_storymapjs\StoryMapJS;

/**
 * Defines a StoryMap.
 */
class Storymap implements StorymapInterface {

  /**
   * The default width.
   *
   * @var string
   */
  protected $width = '1300px';

  /**
   * The default height.
   *
   * @var string
   */
  protected $height = '1000px';

  /**
   * The storymap language.
   *
   * @var string
   */
  protected $language = 'EN';

  /**
   * The storymap type.
   *
   * @var string
   */
  protected $mapType = "stamen:toner-lines";

  /**
   * The storymap type.
   *
   * @var string
   */
  protected $mapSubdomains;

  /**
   * The storymap map background color.
   *
   * @var string
   */
  protected $mapBackgroundColor;

  /**
   * The storymap map_as_image mode.
   *
   * @var bool
   */
  protected $mapAsImage = FALSE;

  /**
   * The storymap map_as_image mode.
   *
   * @var bool
   */
  protected $callToAction;

  /**
   * The map's array of slides.
   *
   * @var \Drupal\views_storymapjs\StoryMapJS\SlideInterface[]
   */
  protected $slides = [];

  /**
   * {@inheritdoc}
   */
  public function setType($map_type) {
    $this->mapType = $map_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setMapSubdomains($map_subdomains) {
    $this->mapSubdomains = $map_subdomains;
  }

  /**
   * {@inheritdoc}
   */
  public function setHeight($height) {
    $this->height = $height;
  }

  /**
   * {@inheritdoc}
   */
  public function setWidth($width) {
    $this->width = $width;
  }

  /**
   * {@inheritdoc}
   */
  public function setCallToAction($call_to_action) {
    $this->callToAction = $call_to_action;
  }

  /**
   * {@inheritdoc}
   */
  public function addSlide(SlideInterface $slide) {
    $this->slides[] = $slide;
  }

  /**
   * {@inheritdoc}
   */
  public function getSlides() {
    return $this->slides;
  }

  /**
   * {@inheritdoc}
   */
  public function getStoryMapOptions() {
    $options = [];
    if ($this->mapType) {
      $options["map_type"] = $this->mapType;
    }
    if ($this->mapSubdomains) {
      $options["map_subdomains"] = $this->mapSubdomains;
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildArray() {
    $map["width"] = $this->width;
    $map["height"] = $this->height;
    $map["storymap"] = [];
    $map["storymap"]["language"] = $this->language;
    if ($this->callToAction) {
      $map["storymap"]["call_to_action"] = TRUE;
      $map["storymap"]["call_to_action_text"] = $this->callToAction;
    }
    if ($this->mapType) {
      $map["storymap"]["map_type"] = $this->mapType;
    }
    if ($this->mapSubdomains) {
      $map["storymap"]["map_subdomains"] = $this->mapSubdomains;
    }
    if ($this->mapAsImage) {
      $map["storymap"]["map_as_image"] = $this->mapAsImage;
    }

    $slides = [];
    foreach ($this->slides as $slide) {
      $slides[] = $slide->buildArray();
    }
    $map["storymap"]['slides'] = $slides;

    return $map;
  }

}
