<?php

namespace Drupal\views_storymapjs\StoryMapJS;

/**
 * Defines a StoryMap slide.
 */
class Slide implements SlideInterface {

  /**
   * The slide type.
   *
   * @var string
   */
  protected $type;

  /**
   * The location latitude.
   *
   * @var string
   */
  protected $locationLat;

  /**
   * The location longitude.
   *
   * @var string
   */
  protected $locationLon;

  /**
   * The location zoom.
   *
   * @var string
   */
  protected $locationZoom;

  /**
   * The slide date.
   *
   * @var string
   */
  protected $date;

  /**
   * The slide headline.
   *
   * @var string
   */
  protected $headline;

  /**
   * The slide text.
   *
   * @var string
   */
  protected $text;

  /**
   * The slide media url.
   *
   * @var string
   */
  protected $mediaUrl;

  /**
   * The slide media caption.
   *
   * @var string
   */
  protected $mediaCaption;

  /**
   * The slide media credit.
   *
   * @var string
   */
  protected $mediaCredit;

  /**
   * Constructs a new Slide object.
   *
   * @param string $type
   *   The type of the slide.
   */
  public function __construct($type = NULL) {
    if (!empty($type)) {
      $this->type = $type;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setLocation($location_lat, $location_lon, $location_zoom = NULL) {
    $this->locationLat = $location_lat;
    $this->locationLon = $location_lon;
    if (!empty($location_zoom)) {
      $this->locationZoom = $location_zoom;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDate($date) {
    $this->date = $date;
  }

  /**
   * {@inheritdoc}
   */
  public function setHeadline($headline) {
    $this->headline = $headline;
  }

  /**
   * {@inheritdoc}
   */
  public function setText($text) {
    $this->text = $text;
  }

  /**
   * {@inheritdoc}
   */
  public function setMedia($media_url, $media_credit, $media_caption) {
    $this->mediaUrl = $media_url;
    if (!empty($media_credit)) {
      $this->mediaCredit = $media_credit;
    }
    if (!empty($media_caption)) {
      $this->mediaCaption = $media_caption;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildArray() {
    $slide = [];

    if (!empty($this->type)) {
      $location['type'] = $this->type;
    }
    if (!empty($this->date)) {
      $location['date'] = $this->date;
    }
    if (!empty($this->headline)) {
      $location['text']['headline'] = $this->headline;
    }
    $location['text']['text'] = !empty($this->text) ? $this->text : "";
    $location['location']['line'] = "true";
    if (!empty($this->locationLat)) {
      $location['location']['lat'] = $this->locationLat;
    }
    if (!empty($this->locationLon)) {
      $location['location']['lon'] = $this->locationLon;
    }
    if (!empty($this->locationZoom)) {
      $location['location']['zoom'] = $this->locationZoom;
    }
    if (!empty($this->mediaUrl)) {
      $location['media']['url'] = $this->mediaUrl;
    }
    if (!empty($this->mediaCaption)) {
      $location['media']['caption'] = $this->mediaCaption;
    }
    if (!empty($this->mediaCredit)) {
      $location['media']['credit'] = $this->mediaCredit;
    }

    // Filter any empty values before returning.
    return array_filter($location);
  }

}
